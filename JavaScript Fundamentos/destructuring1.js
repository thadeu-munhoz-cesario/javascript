//novo recurso do Es2015

const pessoa = {
    nome: 'Ana',
    sobrenome:'Paula',
    idade: 5,
    endereco: {
        logradouro: 'Rua ABC',
        numero: 1000
    }
}

const { nome, idade } = pessoa;
console.log(nome, idade);

const {nome:n, idade:i} = pessoa;
console.log(n,i);

const { sobrenome = 'Não definido', bemHumorada = true} = pessoa;
console.log(sobrenome, bemHumorada);

const { endereco: {logradouro,  numero, cep}} = pessoa;
console.log(logradouro, numero, cep);