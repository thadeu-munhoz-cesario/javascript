function rand([min = 0, max = 1000]){
    if(min > max) [min, max] = [max, min];
    const valor = Math.random() * (max - min) + min;
    return Math.floor(valor);
}

console.log(rand([50,40])); /* Ente 40 e 50 */
console.log(rand([992])); /* Entre 992 e 1000 */
console.log(rand([,10])); /* Entre 0 e 10 */
console.log(rand([])); /* Entre 0 e 1000 */

try{
    console.log(rand()); /* Vai gerar erro */
}catch(e){
    console.log("gerou erro, pois está desestruturando algo nulo");
}
/* Então lembrando que desestruturação é uma funcionalidade para extrair rapidamente dados de um
 * array ou de um objeto.
 */