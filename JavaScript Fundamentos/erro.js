function tratarErroELancar(erro){
    //throw new Error('Erro encaminhado para equipe de Suporte');
    throw{
        nome: erro.name,
        msg: erro.message,
        date: new Date
    }
}

function imprimirNomeGritado(obj){
    try{
        console.log(obj.name.toUpperCase() + '!!!');
    }catch(e){
        tratarErroELancar(e);
    }finally{
        console.log("'finally': Sempre será chamado, inpendente de gerar erro ou não!!")
    }
}

const obj = {nome: 'Roberto'};
imprimirNomeGritado(obj);