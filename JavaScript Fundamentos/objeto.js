/* Objeto em JavaScript é uma coleção de chave e valor.
 * Um Objeto {}       Um array []
 */

 /* Veja que o atributo nome foi criado dinamicamente  */
const prod1 = {};
prod1.nome = "Celular Ultra Mega";
prod1.preco = 4889.90;
prod1['Desconto Legal'] = 0.40; //evitar atributos com espaço.
console.log(prod1);

const prod2 = {
    nome: 'Camisa Polo',
    preco: 79.90,
    obj:{
        blabla: 1,
        obj:{
            blabla: 2
        }
    }
};
console.log(prod2)
/*Objeto é um conjunto de pares chave e valor*/