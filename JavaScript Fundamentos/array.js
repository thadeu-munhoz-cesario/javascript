const valores = [7.7, 8.9, 6.3, 9.2];
console.log(valores[0], valores[3]); 

/* Diferente da maioria das linguagens, em javascript não é retornado um erro quando tentamos acessar
 * um elemento inexistente no array, tipo um elemento da posição 4.
 */
console.log(valores[4]);
valores[4] = 10;
console.log(valores[4]);
valores[10] = 5;
console.log(valores);

/*Verificando o tamanho de um array*/
console.log(valores.length);

/* Podemos misturar tipos de valores dentro de um mesmo array */
valores.push({id:3}, false, null, 'teste');
console.log(valores);

/* Retirando o último elemento de um array */
console.log(valores.pop());
delete valores[0];
delete valores[5];
console.log(valores);

console.log(typeof valores);
