let num1 = 1;
let num2 = 2;

const retornarValor = () => {
    console.log(num1);
}

/*Pós fixada */
num1++;
retornarValor();

/*Pré fixada*/
--num1;
retornarValor();

//Veja abaixo o comportamento de um pré-fixado e um pós-fixado.
console.log(++num1 === num2--);

