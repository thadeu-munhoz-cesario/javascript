console.log(Math.ceil(6.1));

const obj1 = {};
obj1.nome = 'Bola';
//obj1['nome'] = 'Patinete'; /*Acessando por string*/

console.log(obj1.nome)

function Obj(nome){
    this.nome = nome;
    this.exec = function(){
        console.log('Executando....');
    }
}
const obj2 = new Obj('Lápis');
console.log(obj2)
console.log(obj2.nome)
obj2.exec();