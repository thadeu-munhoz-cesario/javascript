/* Veja o operador ternário abaixo.
 * É a mesmo cenário que: 
 *
 *    if(nota >= 7){
 *        return 'Aprovado';
 *    }
 *    else{
 *        return 'Reprovado';
 *    }
 *
 *  OBS.: Lembrando que Arrow Function possui o retorno implícito para somente uma linha.
 *  Além disso, como possui um único parâmetro, posso retirar os parenteses. 
 */
const resultado = nota => nota >= 7 ? 'Aprovado' : 'Reprovado';
console.log(resultado(8.5)); //Aprovado
console.log(resultado(5.5)); //Reprovado