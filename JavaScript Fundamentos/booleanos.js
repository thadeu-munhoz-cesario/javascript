/* Diferente das linguagens padrões, o JavaScript consegue trabalhar com booleanos de uma forma diferente. 
 * Em javascript podemos resolver valores para booleanos, utilizando o "!".
 */
isAtivo = true;
console.log(isAtivo);

/* Veja no exemplo abaixo que podemos resolver o valor '1' para true. Veja que utilizamos duas vezes a 
 * operação de negação "!". Isso porque em termos lógicos negar duas vezes, será retornado true.
 */
isAtivo = 1;
console.log(!!isAtivo); 

/* Veja abaixo, todos os valores que retornam para verdadeiro quando negamos duas vezes */
console.log("os verdadeiros...............");
console.log(!!3);
console.log(!!-1);
console.log(!!" ");
console.log(!![]);
console.log(!!{});
console.log(!!Infinity);
console.log(!!(isAtivo = true));

console.log("os falsos....................");
console.log(!!0);
console.log(!!'');
console.log(!!null);
console.log(!!NaN);
console.log(!!undefined);
console.log(!!(isAtivo = false));

console.log("Para fianlizar............")
/* No caso abaixo, veja que estamos trabalhando com o operador lógico "ou". Então, para que a expressão 
 * inteira resolva para "true", basta que apena um caso resolva para true.
 * No caso, será resolvido para true, pois uma string com um espaço, quando negada duas vezes é resolvida
 * para true.
 */
console.log(!!(''||null||0||' '));

/* Com essas validações, podemos verificar se um determinado foi ou não passado para uma string.
 * Por exemplo, analisando o caso abaixo.
 * Temos uma variável chamada nome, que realiza uma operação lógica do tipo OU (||), uma string chamada
 * 'Desconhecido'.
 * Caso nome esteja vazio, ele passará para a string 'Desconhecido' e o retorno será 'true'.
 * Sempre é retornado o valor verdadeiro, então nesse caso seria retornado o valor 'Desconhecido'.
 * Porém se tivesse um nome, ai logo seria resolvido para true e portanto seria retornado.
 */
let nomeOne = 'Thadeu Munhóz Cesário';
console.log(nomeOne || 'Desconhecido');

let nomeTwo = '';
console.log(nomeTwo || 'Desconhecido');
