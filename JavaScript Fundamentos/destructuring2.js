/* Abaixo estamos utilizando o operador destructuring. Basicamente, criamos uma variável "a" 
 * que receberá um elemento de um array.
 */
const [a] = [10];
console.log(a);

/* Veja também nos elementos abaixo que podemos ignorar elementos. Então, veja que o segundo elemento
 * eu não estou desestruturando. Estou desestruturando o primeiro, terceiro, quinto e sexto elemento.
 * Sendo que no caso do sexto elemento, existe um valor padrão para ele. Que no caso é o valor zero.
 */
const [n1, ,n3 , ,n5, n6 = 0] = [10,7,9,8];
console.log(n1,n3,n5,n6);

/* Veja que o primeiro elemento para desestruturar foi ignorado. O segundo elemento do primeiro array,
 * também é um array. E veja que estou ignorando o primeiro elemento deste array aninhado.
 * Considerando portanto somente o último elemento desse array.
 */
const [,[, nota]] = [[1,2],[3,4]];
console.log(nota);