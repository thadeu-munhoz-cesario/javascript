/* Vamos estudar um pouco o conceito de nulo e undefined!
 * Porém, antes de começarmos vamos avaliar a sentença abaixo. Podemos observar que temos uma objeto
 * e estamos atribuindo esse objeto para a constante a.
 * A questão é que 'a' não possui o valor propriamente do objeto, mas sim ele possui o endereço desse 
 * objeto na memória. Então, basicamente 'a' está apontando para esse endereço da memória, está
 * referenciando.
 */
const a = {name: 'Teste'};

/* Partindo do princípio que 'a' possui o endereço da mémória do objeto, quando atribúimos o valor
 * para a constante 'b', ocorrerá o mesmo processo. Essa constante, passará a referênciar 
 * o endereço do objeto que 'a' também está referênciando.
 */
const b = a;

/* Agora vamos fazer um teste. Vamos alterar pela constante 'b' o valor de name, depois vamos validar
 * se a constante 'a' sentiu essa troca.
 */
b.name = 'Thadeu';
console.log(b.name); //Certo, podemos ver que o valor foi alterado.
console.log(a.name); //Veja que 'a' também foi alterado pois foi alterado o valor na memória que está apontando.