const escola = "Cod3r";

/* A função abaixo retonar o valor que está no indice 4 da string.
 * C |0|   o |1|  d |2|  3|3|  r|4|
 */
console.log(escola.charAt(4));

/* Quando tentamos buscar um indice maior que o tamanho total, é retornado vazio.
 */
console.log(escola.charAt(5));

/* Podemos pegar um valor convertendo seu valor para a tabela asc 
 * Veja que no caso abaixo, pegamos o elemento de indice três e convertemos o respectivo valor,
 * para a seu código na tabela ASC. 
 */
console.log(escola.charCodeAt(3));

/* Também podemos procurar um determinado elemento dentro de um texto e retornar o seu índice.
 * Isso através do indexOf
 */
console.log(escola.indexOf("3"));
console.log(escola.indexOf("a")); /* Neste caso retorna -1 pois não há nenhum valor.*/
console.log(escola.indexOf("r"));

/* Substring, estamos dizendo que do indice 1 para frente, queremos todos os elementos. Portanto,
 * o retorno será od3r.
 */
console.log(escola.substring(1));

/* No caso da substring abaixo, estamos dizendo que vamos do indice 1 até o índice 3, sem incluir o 
 * índice final. No caso vamos retornar apenas "od"
 */
console.log(escola.substring(1,3));

/* Métodos de concatenação. 
 */
console.log("A melhor escola de códigos é a: ".concat(escola).concat("!"));

/*Métodos de substuição*/
console.log(escola.replace("3","e"));

/* Transformando uma string em um array
 * Utilizando a função split podemos facilmente transformar uma string em um array.
 */
console.log("Thadeu, Karina, Guilherme, Katharina".split(","));
