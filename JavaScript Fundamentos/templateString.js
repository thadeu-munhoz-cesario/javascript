
const nome = "Rebeca";
const concatenacao = "Olá" + nome + "!";
const template = `
    Olá
    ${nome}!`;

console.log(concatenacao, template);

/* Podemos ver que o template string considera quebras de linha e ainda por cima é capaz de interpolar
 * os valores das variáveis. Além disso, podemos colocar expressões dentro do template string, veja o
 * exemplo abaixo: 
 */

 console.log(`1 + 1 = ${1+1}`);

 /* Podemos também associar dentro de um template string uma função */
const up = texto => texto.toUpperCase();
 

 console.log(`Bem vindo ${up('thadeu')}`)


