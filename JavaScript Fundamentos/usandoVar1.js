/* Outras linguagens de programação possuem um conceito chamado de escopo. Escopo quer dizer, onde as
 * variáveis são visíveis dentro de seu programa. Para boa parte das linguagens as variáveis ficam
 * visíveis apenas dentro do bloco e não fora.
 */
{
    {
        {
            {
                {
                    var sera = 'Será????'
                    console.log(`Dentro do bloco: ${sera}`);
                }
            }
        }
    }
}
console.log(`Fora do bloco: ${sera}`);

function teste() {
    var local = 123;
    try {
        console.log(`Acessou: ${local}`);
    } catch (e) {
        console("Ops, não acessou");
    }
}
teste();
try {
    console.log(`Dentro do bloco: ${local}`);
} catch (e) {
    console.log("Ops, não acessou");
}
