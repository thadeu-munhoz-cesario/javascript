/* 
 * Primeira estratégia para gerar uma valor padrão.
 * Usando o operador lógico 'ou' (||), podemos carregar um valor padrão que resolveu para um valor
 * falso.
 */

function soma1(a, b, c){
    a = a || 1;
    a = a || 1;
    c = c || 1;
    return a + b + c;
}
console.log(soma1(), soma1(3), soma1(1,2,3), soma1(0,0,0));

/* Entretanto, veja que o 'zero' resolve para false. 
 * Portanto, quando tentamos somar três valores zero, o retorno será 3 ao invés de 0.
 * Logo, temos um bug em nossa aplicação.
 */

 /****************************************************************/
 /* Vamos ver agora mais três estratégias para gerar um valor padrão, para uma parâmetro.
  */

function soma2(a,b,c){

    //Segunda estratégia
    a = (a !== undefined ? a : 1);

    //Terceira estratégia
    b = (1 in arguments ? b : 1);

    //Quarta estratégia
    c = (isNaN(c) ? 1 : c);

    return a + b + c;
}
console.log(soma2(), soma2(3), soma2(1,2,3), soma2(0,0,0));

/* Por último vamos verificar utilizando o próprio valor padrão do ECMAScript 2015.
 */
function soma3(a = 1, b = 1, c = 1){
    return a + b + c;
}
console.log(soma3(), soma3(3), soma3(1,2,3), soma3(0,0,0));