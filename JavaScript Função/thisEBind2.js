function Pessoa(){
    this.idade = 0;

    /* Em javascript o 'this' pode variar de acordo com o elemento que está invocando a função.
     */
    const self = this; //Nessa linha this é a instancia que de fato eu desejo apontar.
    setInterval(function(){
        self.idade++;
        console.log(self.idade);
    }/*.bind(this)*/,1000);
    /* Lembrando que em Javascript, uma função possui outras funções associadas à ela.
     * Portanto, podemos através da notação ponto, chamar outra função. Assim, chamamos a função 'bind'
     * e colocamos o 'this' para referenciarmos a função Pessoa.
     */
}

var p = new Pessoa();
console.log(p);