const soma = function(x,y){
    return x + y;
}
const imprimirResultado = function(a, b, operacao = soma){
    console.log(operacao(a,b));
}
imprimirResultado(3,4);
imprimirResultado(3,4,soma);
imprimirResultado(3,4,function(a,b){return a * b});
imprimirResultado(3,4, (a,b) => a/b);
/* Uma arrow function é sempre anônima, mas uma função normal não necessariamente será sempre
 * anônima. 
 */

 /* Outra forma que temos de declararmos uma função anônima é através de um objeto */
const pessoa = {
    cumprimentar: function(){
        return "Olá";
    },
    desculpar: function(){
        return "Desculpe";
    },
    agradecer: function(){
        return "Obrigado";
    }
}

console.log(`${pessoa.cumprimentar()} pessoal!!`);
console.log(`${pessoa.desculpar()} pessoal!!`);
console.log(`${pessoa.agradecer()} pessoal!!`);