//Associando em uma variável
let dobro = function(a){
    return 2 * a;
}
console.log(dobro(2))

//Reescrevendo como uma arrow function (neste caso, possui o retorno implícito para funções de uma única linha)
const arrowDobro = a => 2 * a;
console.log(arrowDobro(2));

/* Outro Exemplo */
let ola = function(){
    return 'Olá!'
}
console.log(ola());

//Reescrevendo para uma arrow function
const arrowOla = () => {
    return 'Olá!'; //Colocando os blocos em uma função arrow, o retorno não fica mais implícito. Portanto precisamos inserir o return.
}
console.log(arrowOla());