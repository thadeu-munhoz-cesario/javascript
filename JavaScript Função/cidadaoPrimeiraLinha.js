/*
 *   Função em JS é First-Class Object (Citiziens)
 *   Higher-order function
 *
 *   Podemos tratar funçao como sendo um dado. Podendo passar uma função como parâmetro,
 *   podendo retornar uma função como resposta de uma função, podendo armazenar uma função
 *   em uma variável.
 */
/***************************************************************/

// Criar de forma literal
function fun1() {

    /* De forma opcional podemos ou não passar um return para uma função em JS.
     * Porém, quando não há um return, será retornado de forma implícita um 'undefined'.
     */
}

/***************************************************************/

// Podemos armazenar em uma variável
const fun2 = function () {

    // Chamando o nome da constante, estaremos invocando a função.
}

/***************************************************************/

// Armazenar em um array
const array = [function(a,b){return a+b;}, 'Olá', fun1, fun2];
console.log(array[0](2,3));

/***************************************************************/

// Armazenar em um atributo de objeto
const obj = {};
obj.falar = function(){return 'Opa';}
console.log(obj.falar());

/***************************************************************/

// Passando uma função como parametro
function run(fun){
    fun();
}
run(function(){console.log("Executando.......")});

/***************************************************************/

// Uma função pode retornar/conter um função
function soma(a,b){
    return function(c){
        return a + b + c;
    }
}
/* Como o retorno da função 'soma' será uma função, então posso seguir o modelo abaixo para
 * passar os três parâmetros.
 */
console.log(soma(10,5)(5));

/***************************************************************/