/* Um ponto interessante do JavaScript e que mesmo se uma função não tem nenhum parâmetro,
 * não significa que não podemos passar parâmetros para ela.
 * Além disso podemos recuperar esses parâmetros, através do atributo 'arguments'.
 * Veja abaixo o exemplo da função soma:
 */

 function soma(){
    let soma = 0;
    for(i in arguments){
        soma += arguments[i];
    }
    return soma;
 }
 console.log(soma(5,5,10,5,5));

 /* Reforçando: 'arguments' é um array interno de uma função que possui todos os argumentos que
  * foram passados.
  */