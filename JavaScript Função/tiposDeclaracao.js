console.log(soma(1,1));  // *** Veja que estou chamando a função aqui!!!!
//console.log(sub(1,1)); // Não irá funcionar pois 'sub' não é uma function expression.

/* Function Declaration
 * No momento em que eu declaro uma função utilizando a function declaration, eu posso se quiser,
 * chamar essa função antes de tê-la escrita. ***
 * Quando definimos uma função dessa forma o interpretador do javascript primeiro irá ler essas funções
 * e depois irá ler seu código. Então não tem problema chamar essa função antes de declarar sua função.
 * Em outras formas não irá funcionar! Somente na function declaration.
 */
function soma(x,y){
    return x + y;
}

 /* Function Expression
  * Função anônima que é armazenada em uma variável. Esse é um dos métodos mais comuns de encontrarmos
  * as funções definidas no dia a dia. 
  */
const sub = function(x,y){
    return x - y;
}
console.log(sub(1,1)); //Só posso chamar a função DEPOIS de declarar ela. Pois ela não segue o mesmo modelo que a function expression.

 /* Named Function Expression
  * Função nomeada que está armazenada em um função.
  * Vantagem: Durante o debug do código, ou quando o stacktrace aparecer o nome da função estará em
  * evidência. Mas é uma forma pouco usada.
  */
const mult = function mult (x,y){
    return x * y;
}
console.log(mult(1,1)); //Podemos chamar somente depois que a função foi declarada.