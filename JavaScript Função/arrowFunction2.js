function Pessoa(){
    this.idade = 0;

    setInterval(() =>{
        /* Veja que o this não irá variar pois estamos trabalhando com uma arrow function.
         * Isso porque em uma arrow o this segue o contexto léxico. Isto é onde foi escrito.
         * neste caso é a função 'Pessoa'. 
         * Essa é uma grande vantagem de funções arrow, pois o this não irá variar.
         */
        this.idade++;
        console.log(this.idade)
    },1000);
}

var p = new Pessoa();
console.log(p);
