let comparaComThis = function(param){
    console.log(this === param);
}
comparaComThis(global);
/* O this em uma função normal, possui um escopo global. 
 * Portanto, veja no exemplo que global é estritamente igual ao this.
 */

const obj = {};
comparaComThis = comparaComThis.bind(obj); 
/* Com o método 'bind', consido manipular para aonde o this referencia. Então, coloquei no bind o
 * objeto 'obj' como parâmetro. Então agora o this não está mais resolvendo para global, mas sim para
 * obj.
 */
comparaComThis(global);
comparaComThis(obj);

/* Em uma arrow function é diferente o comportamento do 'this'. Pois ele sempre respeitará seu contexto
 * léxico. Logo nunca será apontado para o objeto global. 
 */
let comparaComThisArrow = param => console.log(this === param);
comparaComThisArrow(global);
comparaComThisArrow(module.exports);

/* Vamos tentar utilizar o método 'bind' para tentarmos mudar o comportamento de uma arrow function.
 * Será que com o bind podemos mudar o comportamento do this?
 */

 comparaComThisArrow = comparaComThisArrow.bind(obj);
 comparaComThisArrow(obj); //...WooooW é false!! Sim a arrow function é mais forte que o bind.
 comparaComThisArrow(module.exports);
 /* O this em uma função arrow estará SEMPRE associado ao contexto na qual essa função foi escrita.
  * Mesmo se tentarmos manipular com o bind, a arrow function irá ganhar.
  */
