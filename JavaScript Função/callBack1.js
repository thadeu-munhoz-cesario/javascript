const fabricantes = ["Mercedes","Audi","BMW"];

function imprimir(nome, indice){
    console.log(`${indice+1}. ${nome}`);
}

fabricantes.forEach(imprimir);
/* 'imprimir' é uma função de callback, toda vez que o forEach é executado, o 'imprimir' é chamado.*/

 /* Nada mais é do que você passar uma função, e quando um evento acontecer essa função
  * será invocada. Uma dica é estudar o padrão observer.
  */
