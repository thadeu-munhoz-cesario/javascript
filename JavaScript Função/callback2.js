/* Nosso objetivo: 
 * Através do array de notas abaixo, devemos gerar um novo array com notas menores que 7.
 */
const notas = [7.7, 6.5, 5.2, 8.9, 3.6, 7.1, 9.0];

//Vamos la... sem callback
let notasBaixas = [];                // um array vazio para armazenar as notas baixas
for(let i in notas){                 // Usando um for in para correr no array de notas
    if(notas[i] < 7){                // Validando notas menores que 7         
        notasBaixas.push(notas[i]);  // Aplico um 'push' para inserir o determinado elemento no array de notas baixas
    }
}
console.log(notasBaixas);            // Pronto!

//....... Agora vamos com callback
notasBaixas = notas.filter(elementoAtual => elementoAtual < 7);
console.log(notasBaixas);
/* Filter: função na qual percorrera um determinado array e para cada elemento,
 * chamara uma função de callback que foi especificado. Se essa função retornar 'true',
 * significa que esse elemento deve ser inserido no array, se retornar false significa que o elemento
 * deve ser ignorado e não deve ser inserido dentro do array.
 * A função 'filter' NÃO altera o array original. O array original permanece exatamente da mesma
 * forma.
 */