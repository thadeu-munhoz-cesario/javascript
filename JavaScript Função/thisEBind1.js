const pessoa = {
    saudacao: 'Bom dia',
    falar(){
        console.log(this.saudacao);
    }
}
pessoa.falar();
const falar = pessoa.falar;
falar(); //conflito entre paradigmas: funcional e OO.

const falarDePessoa = pessoa.falar.bind(pessoa);
falarDePessoa();
/* O bind define para quem o 'this' será resolvido.
 * Portanto o this sempre estará relacionado ao objeto pessoa quando chamar a função 'falarDePessoa()'.
 * Isso porque, através da função 'bind' conseguimos referênciá-lo.
 */