/*Armazenando um função em uma variável*/
const imprimirSoma = function(a, b){
    console.log(a + b);
}
imprimirSoma(2,3);

/*Armazenando uma função arrow em uma variável*/
const soma = (a, b) => {
    return a + b;
}

console.log(soma(2, 3));

/*Retorno implícito*/
/* Somente nas Arrow functions, temos um conceito chamado de retorno implícito. No retorno implícito,
 * quando a função possui apenas uma única função, não precisamos solicitar o return, pois automaticamente
 * será retornada. Porém, isso acotence somente nas ARROWS FUNCTIONS.
 */
const subtracao = (a, b) => a - b;
console.log(subtracao(5,3));

/*Podemos reduzir ainda mais, caso a função tenha somente um parametro*/
const imprimirSubtracao = a => a - 3;
console.log(imprimirSubtracao(5));